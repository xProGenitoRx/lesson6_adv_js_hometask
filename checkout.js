class Product { //карточка товара
    constructor (id, name, brand, price, volume, category, img) {
      this.id = id;
      this.name = name;
      this.brand = brand
      this.price = price;
      this.volume = volume;
      this.category = category;
      this.img = img;
      this.qty = 0;
    }


    showProduct () {
      let innerHTML = `<img src="${this.img}"><div><h2>${this.name}</h2>
        <span class="description">${this.category} от ${this.brand}. <span>(${this.volume} кг)</span></span>
        <p>ЦЕНА: ${this.price} грн.</p></div>`;
      return innerHTML;
    }

  }

class ProductManager {  //класс управления массивом товаров
    constructor () {
      this.products = new Array();
      this.cartProducts = new Array();
    }

    addProductRange (products) {
      for (let element of products) {
        this.products.push(new Product(element.id, element.name, element.brand, element.price, element.volume, element.category, element.img));
      }
    }

    loadProductRange (url, method) {
      let xhr = new XMLHttpRequest;
      xhr.open("GET", url);
      xhr.send();
      xhr.onload = function () { method (JSON.parse(xhr.response))};
    }

    getProducts (cartList) { //формирование списка всех категорий товаров
      this.cartProducts = this.products.filter( product => {
          for (let key in cartList) {
              if (key == product.id) {
                  product.qty = cartList[key];
                  return true;              }
          }
        });
    }

    static increment(element) { //увеличение кол-ва товара на 1 шт
      if (element.previousElementSibling.value<99) {  +element.previousElementSibling.value++;
      element.parentElement.querySelector('.refresh').style.display = 'inline-block' }
    }

    static decrement(element) { //уменьшение кол-ва товара на 1 шт
      if (element.nextElementSibling.value>1) { +element.nextElementSibling.value--;
      element.parentElement.querySelector('.refresh').style.display = 'inline-block' }
    }

    static checkAmount(element) { //проверка правельности ручного ввода кол-ва
      if (isNaN(+element.value))
        element.value = 1;
      else if (+element.value < 1)
        element.value = 1;
      else if (+element.value >99)
        element.value = 99;
      else
        element.parentElement.querySelector('.refresh').style.display = 'inline-block'
    }

  }

class NavContainer {  //класс управления навигационной панелью
    constructor (container) {
      this.container = container;
    }

    showNav (categories) {
        this.container.innerHTML = '<ul><li> << Вернуться к покупкам</li></ul>'
        this.container.firstChild.addEventListener ('click', _=> window.location = 'index.html' )
    }
  }

class CartManager { //класс для формирования списка товаров на странице
    constructor (container) {
      this.container = container;
      this.cartList = new Object ();
    }

    showCart (products) { //генерация списка товаров на странице
      this.container.innerHTML = '';
      let sum = 0;
      if (products.length == 0) {
        this.container.innerHTML += '<div class="cart-content"><span id="error">Корзина пуста!</span></div>'  }
      else {
        for (let i=0; i<products.length; i++) {
          if (!products[i])   break;
          this.container.innerHTML += '<div class="cart-content">'+products[i].showProduct()+'<h2>Сумма: '+products[i].price * products[i].qty+' грн.</h2>'+
            this.showPurchaseBlock(products[i].qty, products[i].id)+'</div>';
          sum += products[i].price * products[i].qty;
        } }
      this.showCheckoutBar(sum);
    }

    showPurchaseBlock (qty, id) {
    let innerHTML = `<div class="product-purchase">
                      <button onclick="ProductManager.decrement(this)">\u2012</button>
                      <input  onchange="ProductManager.checkAmount(this)" type="text" size="2" value="${qty}">
                      <button onclick="ProductManager.increment(this)">+</button>
                      <button class="refresh" id="${id}" style="display: none">Обновить</button>
                      <div class="remove-product"><img id="${id}" src="store/bin.png" alt=""></div>
                    </div>`;
    return innerHTML;
    }

    showCheckoutBar (sum) {
        let email = localStorage.getItem('email');
        if (email == null)  email = "";
        let checkoutBar = document.createElement('div');
        checkoutBar.className = 'checkout-bar';
        checkoutBar.innerHTML = `Корзина: <span>Итого к оплате: ${sum} грн.</span><button>Подтвердить заказ</button>
        <p>Ваша электронная почта: <input type="text" placeholder="Email" value="${email}"><span> Проверьте ваш заказ:</span></p>`
        this.container.firstChild.before(checkoutBar);
        document.querySelector('.checkout-bar button').addEventListener ('click', this.checkoutSuccess);
    }

    checkoutSuccess () {
      localStorage.removeItem('cartList');
      window.location = 'checkout_success.html';
    }
}

/* */
const bindControls = function () {
  //загрузка списка товаров корзины из localStorage
  cartManager.cartList = JSON.parse( localStorage.getItem('cartList') );

  //Установка навигационной строки
    navContainer.showNav();
  
  //первоначальная прорисовка товаров
  products.getProducts(cartManager.cartList);
  cartManager.showCart(products.cartProducts);
  
  function noDadNo () {
    function setListener (getSelectors, callbackFunc) {
      let selectorsArray = getSelectors ();
      for (let element of selectorsArray) {
        element.addEventListener("click", event => {
          callbackFunc(event);
          localStorage.setItem('cartList', JSON.stringify(cartManager.cartList) );
          products.getProducts(cartManager.cartList);
          cartManager.showCart(products.cartProducts);
          noDadNo ();
        });
      }
    }
    setListener ( _=> document.getElementsByClassName('refresh'), event => {let qty = event.target.parentElement.querySelector('input').value;
             cartManager.cartList[event.target.id] = +qty;} );       
    setListener ( _=> document.querySelectorAll('.remove-product img'), event => { let response = confirm('Хотите удалить этот товар из корзины?');
    if (response == true) { delete cartManager.cartList[event.target.id]; }} )
  }
  noDadNo ();
  
  }
/*  */

var products = new ProductManager();  //массив товаров, временный вариант
var navContainer = new NavContainer (document.querySelector('.nav-bar'));
var cartManager = new CartManager (document.querySelector('.category-products'));

var promise = new Promise ( resolve => products.loadProductRange ('https://next.json-generator.com/api/json/get/VyElZyqR8', resolve) );

promise.then( function (response) { return response } )
  .then( function (response) { products.addProductRange(response) } )
  .then(function () { bindControls () });