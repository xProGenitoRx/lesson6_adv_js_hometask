class UserForm {
    constructor (email, passwords, button) {
        this.email = email;
        this.passwords = passwords;
        this.button = button;
    }
    checkEmail () {
        function checkAmountOfChar (String, char) {
            let count=0, pos=0;
            while (true) {
                pos = String.indexOf(char, pos+1)
                if (pos != -1) count++; 
                else break;
            }
            return count;
        }
        if ( checkAmountOfChar(this.email.value, '@') != 1 || checkAmountOfChar(this.email.value, '.') < 1 ) {
            this.email.style = 'border-color: red; border-style: solid;';
            document.getElementById('emailMessage').innerText = 'Input correct e-mail';
            return false;
        }
        else {
            this.email.style = '';
            document.getElementById('emailMessage').innerText = '';
            return true;
        }
    }

    checkPassword () {
        if ( this.passwords[0].value.length > 11 || this.passwords[0].value.length < 1 ) {
            this.passwords[0].style = 'border-color: red; border-style: solid;';
            document.getElementById('passwordMessage').innerText = 'Password must be more than 1 and less than 11 symbols';
            return false;
        }
        else {
            this.passwords[0].style = '';
            document.getElementById('passwordMessage').innerText = '';
            return true;
        }
    }

    checkConfirmPassword () {
        if (this.passwords[1].value !== this.passwords[0].value) {
            this.passwords[1].style = 'border-color: red; border-style: solid;';
            document.getElementById('confirmPasswordMessage').innerText = 'Both passwords must be the same';
            return false;
        }
        else {
            this.passwords[1].style = '';
            document.getElementById('confirmPasswordMessage').innerText = '';
            return true;
        }
    }

    Login () {
        if (this.checkEmail() && this.checkPassword() && ((this.passwords[1].offsetParent) ? this.checkConfirmPassword() : true)) {
            localStorage.setItem('email', this.email.value);
            window.location = 'index.html';
        }
    }

    setRegisterForm () {
        document.querySelector('.login-part').style = 'display: none';
        document.querySelector('.register-part').style = 'display: inherit';
    }
    setLoginForm () {
        document.querySelector('.login-part').style = 'display: inherit';
        document.querySelector('.register-part').style = 'display: none';
    }
}

var userForm = new UserForm (document.getElementById('email'),      document.getElementsByClassName('password'),    document.querySelector('button.login'));

const bindControls = function () {
    userForm.email.addEventListener('change', () => userForm.checkEmail()); //проклятый this, потерял кучу времени
    userForm.passwords[0].addEventListener("change", () => userForm.checkPassword());
    userForm.passwords[1].addEventListener("change", () => userForm.checkConfirmPassword());
    userForm.button.addEventListener('click', () => userForm.Login());
    document.querySelector('.login-part a').addEventListener("click", userForm.setRegisterForm);
    document.querySelector('.register-part a').addEventListener("click", userForm.setLoginForm);
}

bindControls (); 