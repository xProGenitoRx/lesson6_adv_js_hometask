class Product { //карточка товара
    constructor (id, name, brand, price, volume, category, img) {
      this.id = id;
      this.name = name;
      this.brand = brand
      this.price = price;
      this.volume = volume;
      this.category = category;
      this.img = img;
    }

    showProduct () {
      let innerHTML = `<img src="${this.img}"><h2>${this.name}</h2>
        <div class="description">${this.category} от ${this.brand}. <span>(${this.volume} кг)</span></div>
        <div>ЦЕНА: ${this.price} грн.</div>`;
      return innerHTML;
    }

  }

class ProductManager {  //класс управления массивом товаров
    constructor () {
      this.products = new Array();
      this.filteredProducts = new Array();
      this.sortedProducts = new Array();
    }

    addProductRange (products) {  //инициализация карточек товара данными из БД
      for (let element of products) {
        this.products.push(new Product(element.id, element.name, element.brand, element.price, element.volume, element.category, element.img));
      }
    }

    loadProductRange (url, method) {  //загрузка БД через XHR
      let xhr = new XMLHttpRequest;
      xhr.open("GET", url);
      xhr.send();
      xhr.onload = function () { method (JSON.parse(xhr.response))};
    }

    getCategoryRange () { //формирование списка всех категорий товаров
      let categoryRange = new Array();
      for (let element of this.products) {
        if(!categoryRange.includes(element.category)) {   categoryRange.push(element.category); }                    
      }
      this.currentCategoryRange = categoryRange;
      return categoryRange;
    }

    filterCategory (chosenCategory) { //фильтрация списка товаров по категории
      this.filteredProducts = Object.assign([], this.products);
      if (chosenCategory != 'Home')
        { this.filteredProducts = this.filteredProducts.filter( product => product.category == chosenCategory); }
      //this.sortedProducts = Object.assign([], this.filteredProducts);
      return this.filteredProducts;
    }

    sorting (sortingKey, direction) { //сортировка списка товаров
      this.sortedProducts = Object.assign([], this.filteredProducts);
      if (direction!==1 && direction!==-1) direction=1;

      this.sortedProducts.sort ( (product1, product2) => {
        if (product1[sortingKey] > product2[sortingKey]) return 1*direction;
        else if (product1[sortingKey] < product2[sortingKey]) return -1*direction;
        else return 0;
      })
      return this.sortedProducts;
    }

    static increment(element) { //увеличение кол-ва товара на 1 шт
      if (element.previousElementSibling.value<99) +element.previousElementSibling.value++; 
    }

    static decrement(element) { //уменьшение кол-ва товара на 1 шт
      if (element.nextElementSibling.value>1) +element.nextElementSibling.value--; 
    }

    static checkAmount(element) { //проверка правельности ручного ввода кол-ва
      if (isNaN(+element.value))
        element.value = 1;
      else if (+element.value < 1)
        element.value = 1;
      else if (+element.value >99)
        element.value = 99;
    }

    setArrays () {
      Object.assign(this.filteredProducts, this.products); Object.assign(this.sortedProducts, this.filteredProducts);
    }
  }

class NavContainer {  //класс управления навигационной панелью
    constructor (container) {
      this.container = container;
    }

    showNav (categories) {
      let innerHTML = '<ul><li>Home</li>'
      for (let i=0; i<categories.length; i++) {
        innerHTML += '<li>'+categories[i]+'</li>';
      }
      this.container.innerHTML = innerHTML+'</ul>';
      this.categories = this.container.firstChild.childNodes;
    }

    showFilter () { //создаем панель с элементами сортировки
      let newBar = document.createElement('div'); 
      newBar.innerText = 'Cортировать: ';
      newBar.className = 'filter-bar';
      this.container.appendChild(newBar);

      let values = ["по имени от А до Я", "по имени от Я до А", "от дешевых к дорогим", "от дорогих к дешевым"];
      this.filterBar = new Array(); //создаем массив для кнопок сортировки
      for (let i=0; i<4; i++) {
        this.filterBar[i] = document.createElement('div');
        this.filterBar[i].innerText = values[i];
        newBar.appendChild(this.filterBar[i]);
      }

      let textContainer = document.createElement('span');
      textContainer.innerText = ' Показать: '
      newBar.appendChild(textContainer);
      
      this.qtySelect = document.createElement('select');
      this.qtySelect.innerHTML += `<option value="6" selected>6</option>
                            <option value="12">12</option>
                            <option value="24">24</option>`
      newBar.appendChild(this.qtySelect);
    }

  }

class CategoryManager { //класс для формирования списка товаров на странице
    constructor (container) {
      this.container = container;
      this.qtyDisplayed = 6;
      this.categoryPages = new Array ();
      this.currentPage = 0;
      this.cartList = {};
    }

    showCategory (products) { //генерация списка товаров на странице
      this.container.innerHTML = '';
      for (let i=this.currentPage*this.qtyDisplayed; i<(this.qtyDisplayed*(this.currentPage+1)); i++) {
        if (!products[i])   break;
        this.container.innerHTML += '<div class="product-content">'+products[i].showProduct()+this.showPurchaseBlock(products[i].id)+'</div>';
      }
      this.purchase ();
      this.showPagination (products);
      this.showCart();
    }

    showPurchaseBlock (id) {
    let innerHTML = `<div class="product-purchase">
                      <button onclick="ProductManager.decrement(this)">\u2012</button>
                        <input  onchange="ProductManager.checkAmount(this)" type="text" size="2" value="1">
                      <button onclick="ProductManager.increment(this)">+</button>
                      <div class="checkout" id="${id}">КУПИТЬ</button>
                    </div>`;
      return innerHTML;
    }

    showPagination (products) { //создаем панель со страницами
      if (document.querySelector('.pagination'))
        this.container.parentNode.removeChild(document.querySelector('.pagination'));
      let pageBar = document.createElement ('div');
      pageBar.className = 'pagination';
      this.container.parentNode.appendChild(pageBar);
      let prvPageButton = document.createElement('span');
      prvPageButton.innerText = '<'
      prvPageButton.addEventListener('click', _ => { if (this.currentPage-1>=0) this.currentPage--;    this.showCategory( products); } );
      let numberOfPages = products.length / this.qtyDisplayed;
      pageBar.appendChild(prvPageButton);
      for (let i=0; i < numberOfPages; i++) {
        this.categoryPages[i] = document.createElement('span'); //установка переключателя страниц
        this.categoryPages[i].innerText = i+1;
        this.categoryPages[i].addEventListener('click', _ => { this.currentPage = i;    this.showCategory( products); } );
        pageBar.appendChild(this.categoryPages[i]);
      }
      let nxtPageButton = document.createElement('span');
      nxtPageButton.innerText = '>'
      nxtPageButton.addEventListener('click', _ => { if (this.currentPage+1 < numberOfPages) this.currentPage++;    this.showCategory( products); } );
      pageBar.appendChild(nxtPageButton);
    }

    purchase () { //обработка нажатия кнопки покупки: алерт и добавление в харнилище
      let purchaseButton = document.getElementsByClassName('checkout');
      for (let element of purchaseButton) {
        element.addEventListener('click', event => {
          let qty = +event.target.previousElementSibling.previousElementSibling.value
          alert('Вы приобрели '+event.target.parentElement.parentElement.querySelector('h2').innerText+' - '+qty+' шт.');
          if (!this.cartList[event.target.id]) {
            this.cartList[event.target.id] = qty; }
          else {
             this.cartList[event.target.id] += qty; }
          this.showCart();
          localStorage.setItem('cartList', JSON.stringify(this.cartList) );
        });
      }
    }

    showCart () {
      let shoppingCart = document.querySelector('.shopping-cart');
      if (shoppingCart)
        shoppingCart.parentElement.removeChild(shoppingCart);
      let cartContainer = document.createElement ('div');
      cartContainer.className = 'shopping-cart';
      let qtyPurchased = 0;
      for (let key in this.cartList) {
        qtyPurchased += +this.cartList[key];
      }
      if (qtyPurchased == 0) {
        cartContainer.innerHTML = '<img src="store/shopping-cart.png" alt=""><span> Корзина пуста</span>';  }
      else {
        let string
        if ( qtyPurchased.toString().split('').pop() == 1) { string = ' товар';  }
        else if ( qtyPurchased.toString().split('').pop() < 5) { string = ' товара'; }
        else {  string = ' товаров' };
        cartContainer.innerHTML = '<img src="store/shopping-cart.png" alt=""><span> В корзине:<br>'+qtyPurchased+string+'</span>';  }
      this.container.appendChild(cartContainer);
      document.querySelector('.shopping-cart').addEventListener('click', _=> window.location = 'cart.html');
    }
}

/* */
const bindControls = function () {
  let storage = JSON.parse( localStorage.getItem('cartList') );
  if (storage)  {
    catergoryManager.cartList = storage;  }

//Установка навигационной строки
  navContainer.showNav(products.getCategoryRange());
  navContainer.showFilter();
  for (let element of navContainer.categories) {
    element.addEventListener("click", () => { 
      catergoryManager.showCategory( products.filterCategory (element.innerText))
      navContainer.qtySelect.selectedIndex = 0;     //установление кол-во товаров на странице по умолчанию при переключении
      catergoryManager.qtyDisplayed = navContainer.qtySelect[navContainer.qtySelect.selectedIndex].value; });  //категории
      catergoryManager.currentPage = 0; //возврат на 1 страницу
  }

//установка строки для сортировки
  let filterValues = [['name', 1],['name', -1],['price', 1],['price', -1]];
  for (let i=0; i<navContainer.filterBar.length; i++) {
    navContainer.filterBar[i].addEventListener("click", event => {
      catergoryManager.showCategory( products.sorting (filterValues[i][0], filterValues[i][1]))
      for (let i=0; i<navContainer.filterBar.length; i++) { //устанавливаем
        navContainer.filterBar[i].className = ''; }         //селектор активным
      event.target.className = "active";                 //при нажатии work in progress
    });
  }

//установка селектора кол-ва товаров на странице
  navContainer.qtySelect.addEventListener ('change', event => {
    catergoryManager.qtyDisplayed = event.target.options [ event.target.selectedIndex ].text;
    catergoryManager.showCategory(products.sortedProducts); } );
  
//первоначальная прорисовка товаров
products.setArrays();
catergoryManager.showCategory(products.products);

//hover подсветка
  catergoryManager.container.addEventListener("mouseover", (event)=> {
    if (event.target.className == 'product-content')
      event.target.style.background = 'rgba(122, 197, 124, 0.5)';
    });
  catergoryManager.container.addEventListener("mouseout", (event)=> {
    if (event.target.className == 'product-content')
      event.target.style.background = '';
  });
}

/*  */

var products = new ProductManager();  //массив товаров, временный вариант
var navContainer = new NavContainer (document.querySelector('.nav-bar'));
var catergoryManager = new CategoryManager (document.querySelector('.category-products'));

var promise = new Promise ( resolve => products.loadProductRange ('https://next.json-generator.com/api/json/get/VyElZyqR8', resolve) );

promise.then( function (response) { return response } )
  .then( function (response) { products.addProductRange(response) } )
  .then(function () { bindControls () });